To run this program you will need to download visual studio code.
You will then need to install the python extension.
Next you will need to click clone repository in this GitLab and copy it into VSCode
Then you need to press the run button in the top right of VSCode to run the program.

If you have any errors you may need to use Pip in the command line to install some of the libraries.
The code you need for that can be copy and pasted from here:
 pip install time
 pip install pandas
 pip install networkx
 pip install matplotlib

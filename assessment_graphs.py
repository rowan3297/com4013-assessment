## Depth First Search
## The graph
pygraph = {"9": ["5","14"],
    "5": ["3","6","9"],
    "3": ["1","4","5"], 
    "14": ["11","17","9"],
    "11": ["10","12","14"],
    "17": ["15","19","14"],
    "15": ["17"],
    "1": ["3"],
    "4": ["3"],
    "6": ["5"],
    "10": ["11"],
    "12": ["11"],
    "19": ["17"]
} 

## The stack
visited = []

import time
t1=time.time()
##Algorithm function
def dfs(visited,City):
    if City not in visited:
        print(City)
        visited.append(City)
        for neighbour in pygraph[City]:
            dfs(visited,neighbour)

## Calling the function
dfs(visited,"9")

t2=time.time()
print("Time taken:",t2-t1)
#### Drawing code
## import required libraries
import pandas as pd
import networkx as nx
from matplotlib import pyplot as plt

## set the figure size
plt.rcParams["figure.figsize"] = [7.50, 3.50]

## set the figure layout
plt.rcParams["figure.autolayout"] = True

## create the graph
df = pd.DataFrame({'from': ['9:D', '9:D', '5:D', '5:D', '3:D', '3:D', '14:D', '14:D', '11:D', '11:D', '17:D', '17:D'], 
                                'to': ['5:D', '14:D', '6:D', '3:D', '1:D', '4:D', '11:D', '17:D', '10:D', '12:D', '15:D', '19:D']})

## draw the graph
G = nx.from_pandas_edgelist(df, 'from', 'to')
nx.draw(G, with_labels=True, node_size=100, alpha=1, linewidths=10)
plt.show()